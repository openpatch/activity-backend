FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.7.0

ENV OPENPATCH_SERVICE_NAME=activity
ENV OPENPATCH_AUTHENTIFICATION_SERVICE=https://api.openpatch.app/authentification
ENV OPENPATCH_DB=sqlite+pysqlite:///database.db
ENV OPENPATCH_ORIGIN=*

COPY "." "/var/www/app"

RUN pip3 install -r /var/www/app/requirements.txt
