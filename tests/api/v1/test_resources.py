from unittest.mock import patch
from tests.base_test import BaseTest
from tests.mock import members, likes, follows
from openpatch_activity.models.like import Like
from openpatch_activity.rabbitmq import rabbit
import uuid


class TestResources(BaseTest):
    def test_get(self):
        response = self.client.get(f"/v1/resources/{likes[0]['resource']}")
        self.assertEqual(response.status_code, 200)
        self.assertIn("likes_count", response.get_json())
        self.assertTrue(response.get_json()["likes_count"] > 0)
        self.assertIn("is_liked", response.get_json())
        self.assertFalse(response.get_json()["is_liked"])
        self.assertIn("follows_count", response.get_json())
        self.assertTrue(response.get_json()["follows_count"] == 0)
        self.assertIn("is_followed", response.get_json())
        self.assertFalse(response.get_json()["is_followed"])

    def test_get_likes(self):
        response = self.client.get(f"/v1/resources/{likes[0]['resource']}/likes")
        self.assertEqual(response.status_code, 200)
        self.assertIn("likes", response.get_json())
        public_likes = response.get_json()["likes"]
        self.assertTrue(len(public_likes) > 0)
        self.assertIn("is_liked", response.get_json())
        self.assertFalse(response.get_json()["is_liked"])

        response = self.client.get(
            f"/v1/resources/{likes[0]['resource']}/likes",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("likes", response.get_json())
        user_likes = response.get_json()["likes"]
        self.assertIn("is_liked", response.get_json())
        self.assertTrue(response.get_json()["is_liked"])
        self.assertEqual(public_likes, user_likes)

    @patch.object(rabbit, "publish_activity")
    def test_post_like(self, mock_publish_activity):
        response = self.client.post(
            f"/v1/resources/{likes[0]['resource']}/like",
            json={"type": "item"},
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(mock_publish_activity.called)
        self.assertIn("like_id", response.get_json())

        response = self.client.post(
            f"/v1/resources/{uuid.uuid4()}/like",
            json={"type": "member"},
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(mock_publish_activity.called)
        self.assertIn("like_id", response.get_json())

    def test_delete_like(self):
        response = self.client.delete(
            f"/v1/resources/{likes[0]['resource']}/like",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {})

        response = self.client.delete(
            f"/v1/resources/{members[1]['id']}/like", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 404)

    def test_get_follows(self):
        response = self.client.get(f"/v1/resources/{follows[0]['resource']}/follows")
        self.assertEqual(response.status_code, 200)
        self.assertIn("follows", response.get_json())
        public_follows = response.get_json()["follows"]
        self.assertTrue(len(public_follows) > 0)
        self.assertIn("is_followed", response.get_json())
        self.assertFalse(response.get_json()["is_followed"])

        response = self.client.get(
            f"/v1/resources/{follows[0]['resource']}/follows",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("follows", response.get_json())
        user_follows = response.get_json()["follows"]
        self.assertIn("is_followed", response.get_json())
        self.assertTrue(response.get_json()["is_followed"])
        self.assertEqual(public_follows, user_follows)

    @patch.object(rabbit, "publish_activity")
    def test_post_follow(self, mock_publish_activity):
        response = self.client.post(
            f"/v1/resources/{members[0]['id']}/follow",
            json={"type": "member"},
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(mock_publish_activity.called)
        self.assertIn("follow_id", response.get_json())

        response = self.client.post(
            f"/v1/resources/{members[1]['id']}/follow",
            json={"type": "member"},
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(mock_publish_activity.called)
        self.assertIn("follow_id", response.get_json())

    def test_delete_follow(self):
        response = self.client.delete(
            f"/v1/resources/{members[0]['id']}/follow",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {})

        response = self.client.delete(
            f"/v1/resources/{members[1]['id']}/follow",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 404)
