from unittest.mock import patch
from tests.base_test import BaseTest
from tests.mock import members, likes
from openpatch_activity.models.like import Like
from openpatch_activity.rabbitmq import rabbit
import uuid


class TestMembers(BaseTest):
    def test_get_likes(self):
        response = self.client.get(
            f"/v1/members/{members[2]['id']}/likes", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("likes", response.get_json())
        self.assertTrue(len(response.get_json()["likes"]) > 0)

        response = self.client.get(
            f"/v1/members/{members[0]['id']}/likes", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("likes", response.get_json())
        self.assertTrue(len(response.get_json()["likes"]) == 0)

    def test_get_follows(self):
        response = self.client.get(
            f"/v1/members/{members[2]['id']}/follows", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("follows", response.get_json())
        self.assertTrue(len(response.get_json()["follows"]) > 0)

        response = self.client.get(
            f"/v1/members/{members[0]['id']}/follows",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("follows", response.get_json())
        self.assertTrue(len(response.get_json()["follows"]) == 0)

