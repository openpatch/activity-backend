from unittest.mock import patch
from tests.base_test import BaseTest
from tests.mock import members


class TestActivities(BaseTest):
    def test_get_activities_following(self):
        response = self.client.get("/v1/activities", headers=self.fake_headers["user"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("activities", response.get_json())
        self.assertTrue(len(response.get_json()["activities"]) > 0)

        for activity in response.get_json()["activities"]:
            self.assertEqual(
                activity["member"],
                {
                    "id": members[0]["id"],
                    "username": None,
                    "avatar_id": None,
                    "full_name": None,
                },
            )

    def test_get_activities_no_following(self):
        response = self.client.get("/v1/activities", headers=self.fake_headers["admin"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("activities", response.get_json())
        self.assertTrue(len(response.get_json()["activities"]) == 0)
