from openpatch_core.database import db
from openpatch_activity.api.v1.schemas.activity import ACTIVITIES_SCHEMA
from openpatch_activity.api.v1.schemas.like import LIKES_SCHEMA
from openpatch_activity.api.v1.schemas.follow import FOLLOWS_SCHEMA
from openpatch_activity.api.v1.schemas.member import MEMBERS_SCHEMA
from openpatch_activity.api.v1.schemas.resources import RESOURCES_SCHEMA
import uuid


members = [
    {"id": "250512d6-16e8-4161-8ac2-43501a7efe28"},
    {"id": str(uuid.uuid4())},
    {"id": "749e10b4-9545-40ad-9f8a-8e41f08c817b"},
    {"id": str(uuid.uuid4())},
    {"id": str(uuid.uuid4())},
]

resources = [
    {"id": str(uuid.uuid4()), "type": "item"},
    {"id": str(uuid.uuid4()), "type": "item"},
    {"id": str(uuid.uuid4()), "type": "item"},
    {"id": str(uuid.uuid4()), "type": "item"},
    {"id": str(uuid.uuid4()), "type": "item"},
    {"id": members[0]["id"], "type": "member"},
]

activities = [
    {"type": "create", "resource": resources[0], "member": members[0]},
    {"type": "update", "resource": resources[1], "member": members[0]},
    {"type": "like", "resource": resources[2], "member": members[1]},
    {"type": "remove", "resource": resources[3], "member": members[0]},
    {"type": "block", "resource": resources[4], "member": members[0]},
]

likes = [
    {"resource": resources[0]["id"], "member": members[2]["id"]},
    {"resource": resources[1]["id"], "member": members[2]["id"]},
    {"resource": resources[2]["id"], "member": members[4]["id"]},
]

follows = [{"resource": resources[5]["id"], "member": members[2]["id"]}]


def mock():
    result = MEMBERS_SCHEMA.load(members, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    result = RESOURCES_SCHEMA.load(resources, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    result = ACTIVITIES_SCHEMA.load(activities, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    result = LIKES_SCHEMA.load(likes, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    result = FOLLOWS_SCHEMA.load(follows, session=db.session)
    db.session.add_all(result)
    db.session.commit()
