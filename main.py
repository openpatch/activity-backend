import os
import unittest

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from coverage import Coverage
from flask_migrate import Migrate
from openpatch_core.database import db, gt
from openpatch_activity import create_app
from tests.mock import mock as mock_db


environment = os.getenv("OPENPATCH_MODE", "development")
app = create_app(environment)

migrate = Migrate(app, db, version_table=gt("version"))

sentry_dsn = os.getenv("SENTRY_DSN", None)

if sentry_dsn:
    sentry_sdk.init(
        dsn=sentry_dsn, integrations=[FlaskIntegration()], environment=environment
    )


@app.cli.command()
def test():
    tests = unittest.TestLoader().discover("tests", pattern="test*.py")
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 0


@app.cli.command()
def mock():
    db.drop_all()
    db.create_all()
    mock_db()
    db.session.commit()


if __name__ == "__main__":
    if os.getenv("OPENPATCH_MOCK"):
        with app.app_context():
            db.drop_all()
            db.create_all()
            mock_db()
            db.session.commit()

    app.run(host="0.0.0.0", port=80)

