from openpatch_core.database import db
from openpatch_activity.models import activity, follow, like, member, resource
from sqlalchemy import orm

orm.configure_mappers()
