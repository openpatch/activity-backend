from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
import uuid


class Follow(Base):
    __tablename__ = gt("follow")
    __table_args__ = (
        db.UniqueConstraint("member_id", "resource_id", name="_member_resource_uc"),
    )

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)

    resource_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("resource"))), nullable=False
    )
    resource = db.relationship("Resource", back_populates="follows")

    member_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("member"))), nullable=False
    )
    member = db.relationship("Member")
