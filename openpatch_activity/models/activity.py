from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
import uuid
import enum


"""
create = 1
update = 2
delete = 3
follow = 4
add = 5
remove = 6
like = 7
undo = 8
block = 9
comment = 10
"""


class Activity(Base):
    __tablename__ = gt("activity")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    type = db.Column(db.String(64))

    resource_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("resource"))), nullable=False
    )
    resource = db.relationship("Resource")

    member_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("member"))), nullable=False
    )
    member = db.relationship("Member")
