from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
import uuid


class Resource(Base):
    __tablename__ = gt("resource")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    type = db.Column(db.String(256), nullable=False)
    data = db.Column(db.JSON)

    likes = db.relationship("Like", back_populates="resource")
    follows = db.relationship("Follow", back_populates="resource")
    activities = db.relationship("Activity", back_populates="resource")

    @classmethod
    def get_or_create(cls, id, type, data):
        resource = cls.query.get(id)

        if not resource:
            resource = cls(id=id, type=type)
            db.session.add(resource)

        resource.data = data
        resource.type = type

        db.session.commit()
        return resource
