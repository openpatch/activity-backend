"""
@apiDefine jwt
@apiHeader {String} Authorization JWT Token
@apiHeaderExample {json} Authorization-Example: 
    {
        "Authorization": "Bearer aaaaaaaa.bbbbbbb.cccccc"
    }
"""

"""
@apiDefine elastic_query
@apiParam (Query) {Object} query
@apiParam (Query) {Object} query.filter Allowed operators: like, equals, is_null, is_not_null, gt, gte, lt, lte, in, not_in, not_equal_to
@apiParam (Query) {Object} query.sort Allowed operators: asc, desc
@apiParam (Query) {Number} query.limit Limit the number of responses
@apiParam (Query) {Number} query.offset Offset the start
"""
