from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_core.jwt import jwt_required, get_jwt_claims
from openpatch_activity.models.activity import Activity
from openpatch_activity.models.member import Member
from openpatch_activity.models.follow import Follow
from openpatch_activity.api.v1 import api, errors
from openpatch_activity.api.v1.schemas.activity import ACTIVITIES_SCHEMA

base_url = "/activities"


@jwt_required
@api.route(base_url, methods=["GET"])
def get_activities():
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    activity_query, count, page = Activity.elastic_query(
        request.args.get("query", "{}")
    )
    activity_query = activity_query.join(
        Follow, Follow.resource_id == Activity.member_id
    ).filter(
        db.and_(Follow.member == member, Activity.type.notin_(["block", "remove"]))
    )
    count = activity_query.count()
    activity_query = page(query=activity_query)
    activities = activity_query.all()

    return jsonify(
        {"activities": ACTIVITIES_SCHEMA.dump(activities), "activities_count": count}
    )
