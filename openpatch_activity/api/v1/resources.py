from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_core.jwt import jwt_required, get_jwt_claims
from openpatch_activity.rabbitmq import rabbit
from openpatch_activity.models.follow import Follow
from openpatch_activity.models.like import Like
from openpatch_activity.models.resource import Resource
from openpatch_activity.models.activity import Activity
from openpatch_activity.models.member import Member
from openpatch_activity.api.v1 import api, errors
from openpatch_activity.api.v1.schemas.resources import RESOURCE_SCHEMA
from openpatch_activity.api.v1.schemas.member import MEMBER_SCHEMA
from openpatch_activity.api.v1.schemas.follow import FOLLOWS_SCHEMA
from openpatch_activity.api.v1.schemas.like import LIKES_SCHEMA
from openpatch_activity.rabbitmq import rabbit

base_url = "/resources"


@api.route(base_url + "/<resource_id>", methods=["GET"])
def resource(resource_id):
    jwt_claims = get_jwt_claims(optional=True)
    resource = Resource.query.get(resource_id)
    likes_count = Like.query.filter_by(resource=resource).count()
    follows_count = Follow.query.filter_by(resource=resource).count()
    is_liked = False
    is_followed = False
    if jwt_claims:
        member = Member.get_or_create(jwt_claims)
        is_liked = (
            Like.query.filter_by(resource=resource, member=member).first() is not None
        )
        is_followed = (
            Follow.query.filter_by(resource=resource, member=member).first() is not None
        )

    return (
        jsonify(
            {
                "likes_count": likes_count,
                "follows_count": follows_count,
                "is_liked": is_liked,
                "is_followed": is_followed,
            }
        ),
        200,
    )


@jwt_required
@api.route(base_url + "/<resource_id>/like", methods=["POST", "DELETE"])
def resource_like(resource_id):
    if request.method == "POST":
        return post_resource_like(resource_id)
    elif request.method == "DELETE":
        return delete_resource_like(resource_id)


def post_resource_like(resource_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    resource_json = request.get_json()
    if not resource_json:
        return errors.no_json()

    if not resource_json.get("type"):
        return errors.invalid_json("Missing type")

    resource = Resource.get_or_create(
        resource_id, resource_json.get("type"), resource_json.get("data")
    )

    like = Like.query.filter_by(member=member, resource=resource).first()

    if not like:
        like = Like(resource=resource, member=member)
        db.session.add(like)
        db.session.commit()

        resource_json = RESOURCE_SCHEMA.dump(resource)
        del resource_json["likes"]
        del resource_json["follows"]
        member_json = MEMBER_SCHEMA.dump(member)
        del member_json["liked"]
        del member_json["following"]

        rabbit.publish_activity("like", resource_json, member_json)

    return jsonify({"like_id": str(like.id)})


def delete_resource_like(resource_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)
    resource = Resource.query.get(resource_id)
    like = Like.query.filter_by(member=member, resource=resource).first()

    if not like:
        return errors.resource_not_found()

    Activity.query.filter_by(resource=resource, type="like").delete()
    db.session.delete(like)
    db.session.commit()

    return jsonify({})


@api.route(base_url + "/<resource_id>/likes", methods=["GET"])
def resource_likes(resource_id):
    jwt_claims = get_jwt_claims(optional=True)
    likes_query, count, page = Like.elastic_query(request.args.get("query", "{}"))
    likes_query = likes_query.filter_by(resource_id=resource_id)
    likes_count = likes_query.count()
    is_liked = False
    if jwt_claims:
        member = Member.get_or_create(jwt_claims)
        is_liked = likes_query.filter_by(member=member).first() is not None

    likes = page(likes_query)

    return jsonify(
        {
            "likes": LIKES_SCHEMA.dump(likes),
            "likes_count": likes_count,
            "is_liked": is_liked,
        }
    )


@jwt_required
@api.route(base_url + "/<resource_id>/follow", methods=["POST", "DELETE"])
def resource_follow(resource_id):
    if request.method == "POST":
        return post_resource_follow(resource_id)
    elif request.method == "DELETE":
        return delete_resource_follow(resource_id)


def post_resource_follow(resource_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    resource_json = request.get_json()
    if not resource_json:
        return errors.no_json()

    if not resource_json.get("type"):
        return errors.invalid_json("Missing type")

    resource = Resource.get_or_create(
        resource_id, resource_json.get("type"), resource_json.get("data")
    )

    follow = Follow.query.filter_by(member=member, resource=resource).first()

    if not follow:
        follow = Follow(resource=resource, member=member)
        db.session.add(follow)
        db.session.commit()

        resource_json = RESOURCE_SCHEMA.dump(resource)
        del resource_json["likes"]
        del resource_json["follows"]
        del resource_json["activities"]
        member_json = MEMBER_SCHEMA.dump(member)
        del member_json["liked"]
        del member_json["following"]

        rabbit.publish_activity("follow", resource_json, member_json)

    return jsonify({"follow_id": str(follow.id)})


def delete_resource_follow(resource_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)
    resource = Resource.query.get(resource_id)
    follow = Follow.query.filter_by(member=member, resource=resource).first()

    if not follow:
        return errors.resource_not_found()

    Activity.query.filter_by(resource=resource, type="follow").delete()
    db.session.delete(follow)
    db.session.commit()
    return jsonify({})


@api.route(base_url + "/<resource_id>/follows", methods=["GET"])
def resource_follows(resource_id):
    jwt_claims = get_jwt_claims(optional=True)
    follows_query, count, page = Follow.elastic_query(request.args.get("query", "{}"))
    follows_query = follows_query.filter_by(resource_id=resource_id)
    follows_count = follows_query.count()
    is_followed = False
    if jwt_claims:
        member = Member.get_or_create(jwt_claims)
        is_followed = follows_query.filter_by(member=member).first() is not None
    follows = page(follows_query)

    return jsonify(
        {
            "follows": FOLLOWS_SCHEMA.dump(follows),
            "follows_count": follows_count,
            "is_followed": is_followed,
        }
    )
