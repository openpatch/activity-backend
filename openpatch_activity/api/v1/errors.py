from openpatch_core.errors import error_manager as em


def access_not_allowed():
    """
    @apiDefine errors_access_not_allowed
    @apiError (ErrorCode 1) {String} message AccessNotAllowed
    @apiError (ErrorCode 1) {Integer} status_code 403
    @apiError (ErrorCode 1) {Integer} code 1
    """
    return em.make_json_error(
        403, message="Your credentials do not allow access to this resource", code=1
    )


def resource_not_found():
    """
    @apiDefine errors_resource_not_found
    @apiError (ErrorCode 110) {String} message ResourceNotFound
    @apiError (ErrorCode 110) {Integer} status_code 404
    @apiError (ErrorCode 110) {Integer} code 110
    """
    return em.make_json_error(404, message="Resource not found", code=110)


def invalid_json(errors):
    """
    @apiDefine errors_invalid_json
    @apiError (ErrorCode 200) {String} message InvalidJson
    @apiError (ErrorCode 200) {Integer} status_code 400
    @apiError (ErrorCode 200) {Integer} code 200
    """
    return em.make_json_error(
        400, message="Invalid JSON in request body", code=200, details=errors
    )


def no_json():
    """
    @apiDefine errors_no_json
    @apiError (ErrorCode 201) {String} message NoJson
    @apiError (ErrorCode 201) {Integer} status_code 400
    @apiError (ErrorCode 201) {Integer} code 201
    """
    return em.make_json_error(400, message="JSON in request body is missing", code=201)
