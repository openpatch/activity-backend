from marshmallow import fields, EXCLUDE
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from openpatch_core.database import db
from openpatch_activity.models.follow import Follow


class FollowSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Follow
        unkown = EXCLUDE
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()


FOLLOW_SCHEMA = FollowSchema()
FOLLOWS_SCHEMA = FollowSchema(many=True)
