from marshmallow import fields, EXCLUDE
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from openpatch_core.schemas import ma
from openpatch_core.database import db
from openpatch_activity.models.member import Member
from openpatch_activity.api.v1.schemas.follow import FollowSchema
from openpatch_activity.api.v1.schemas.like import LikeSchema


class MemberSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Member
        unkown = EXCLUDE
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()

    following = fields.Nested(FollowSchema, many=True)
    liked = fields.Nested(LikeSchema, many=True)


MEMBER_SCHEMA = MemberSchema()
MEMBERS_SCHEMA = MemberSchema(many=True)
