from marshmallow import fields, EXCLUDE
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from openpatch_core.database import db
from openpatch_core.schemas import ma
from openpatch_activity.models.resource import Resource


class ResourceSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Resource
        unkown = EXCLUDE
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()


RESOURCE_SCHEMA = ResourceSchema()
RESOURCES_SCHEMA = ResourceSchema(many=True)
