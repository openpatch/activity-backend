from marshmallow import fields, EXCLUDE
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from openpatch_core.schemas import ma
from openpatch_activity.models.like import Like
from openpatch_core.database import db


class LikeSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Like
        unkown = EXCLUDE
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()


LIKE_SCHEMA = LikeSchema()
LIKES_SCHEMA = LikeSchema(many=True)
