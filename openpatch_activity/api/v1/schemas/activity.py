import enum
from marshmallow import fields, ValidationError, EXCLUDE
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from openpatch_core.database import db
from openpatch_core.schemas import ma
from openpatch_activity.api.v1.schemas.resources import ResourceSchema
from openpatch_activity.api.v1.schemas.member import MemberSchema
from openpatch_activity.models.activity import Activity


class ActivitySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Activity
        unkown = EXCLUDE
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    resource = ma.Nested(ResourceSchema(only=["type", "id", "data"]))
    member = ma.Nested(MemberSchema(only=["id", "username", "full_name", "avatar_id"]))


ACTIVITY_SCHEMA = ActivitySchema()
ACTIVITIES_SCHEMA = ActivitySchema(many=True)
