from flask import jsonify, request
from openpatch_core.database import db
from openpatch_core.jwt import jwt_required, get_jwt_claims
from openpatch_activity.models.activity import Activity
from openpatch_activity.models.like import Like
from openpatch_activity.models.follow import Follow
from openpatch_activity.models.member import Member
from openpatch_activity.api.v1 import api, errors
from openpatch_activity.api.v1.schemas.follow import FOLLOWS_SCHEMA, FOLLOW_SCHEMA
from openpatch_activity.api.v1.schemas.like import LIKES_SCHEMA, LIKE_SCHEMA

base_url = "/members"


@jwt_required
@api.route(base_url + "/<member_id>/export", methods=["GET"])
def export_member(member_id):
    jwt_claims = get_jwt_claims()
    if not member_id == jwt_claims.get("id"):
        return errors.access_not_allowed()
    member = Member.query.get(jwt_claims.get("id"))
    return jsonify({"member": {}})


@jwt_required
@api.route(base_url + "/<member_id>/delete", methods=["DELETE"])
def delete_member(member_id):
    jwt_claims = get_jwt_claims()
    if not member_id == jwt_claims.get("id"):
        return errors.access_not_allowed()
    member = Member.query.get(jwt_claims.get("id"))
    return jsonify({})


@jwt_required
@api.route(base_url + "/<member_id>/likes", methods=["GET"])
def get_member_likes(member_id):
    jwt_claims = get_jwt_claims()
    if not member_id == jwt_claims.get("id"):
        return errors.access_not_allowed()

    likes_query, count, page = Like.elastic_query(request.args.get("query", "{}"))
    likes_query = likes_query.filter_by(member_id=jwt_claims.get("id"))
    likes_count = likes_query.count()
    likes = page(likes_query)

    return jsonify({"likes": LIKES_SCHEMA.dump(likes), "likes_count": likes_count})


@jwt_required
@api.route(base_url + "/<member_id>/follows", methods=["GET"])
def get_member_follows(member_id):
    jwt_claims = get_jwt_claims()
    if not member_id == jwt_claims.get("id"):
        return errors.access_not_allowed()

    follows_query, count, page = Follow.elastic_query(request.args.get("query", "{}"))
    follows_query = follows_query.filter_by(member_id=jwt_claims.get("id"))
    follows_count = follows_query.count()
    follows = page(follows_query)

    return jsonify(
        {"follows": FOLLOWS_SCHEMA.dump(follows), "follows_count": follows_count}
    )
