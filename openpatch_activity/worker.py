import os
import json
import pika
import requests
import logging
from retry import retry
from marshmallow import ValidationError

from openpatch_core.database import db
from openpatch_activity.api.v1.schemas.activity import ACTIVITY_SCHEMA
from openpatch_activity.models.activity import Activity
from openpatch_activity.models.resource import Resource
from openpatch_activity.models.member import Member

logging.basicConfig(level=logging.INFO)

_DELIVERY_MODE_PERSISTENT = 2


class Worker:
    def __init__(self, app, rabbitmq_host, retry_delay_ms):
        self.app = app
        self.rabbitmq_host = rabbitmq_host
        self.retry_delay_ms = retry_delay_ms

    @retry(pika.exceptions.AMQPConnectionError, delay=5, jitter=(1, 3))
    def consume(self):
        logging.info(self.rabbitmq_host)
        self.connection = pika.BlockingConnection(
            pika.URLParameters(self.rabbitmq_host)
        )
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue="activity_queue", durable=True)

        self.channel.exchange_declare(
            exchange="activity_exchange", exchange_type="direct", durable=True
        )
        self.channel.queue_bind(exchange="activity_exchange", queue="activity_queue")

        self.channel.basic_consume("activity_queue", self.process_activity_queue)
        try:
            self.channel.start_consuming()
            logging.info("Connected to Rabbitmq {}", self.rabbitmq_host)
        except pika.exceptions.ConnectionClosedByBroker:
            logging.warning(
                "Connection to Rabbitmq {} closed by broker", self.rabbitmq_host
            )
        except pika.exceptions.ConnectionClosedByClient:
            logging.warning(
                "Connection to Rabbitmq {} closed by client", self.rabbitmq_host
            )

    def process_activity_queue(self, ch, method, properties, body):
        logging.info("Saving activity {}".format(body))
        try:
            activity_json = json.loads(body)
            version = activity_json["version"]
            type = activity_json["type"]

            with self.app.app_context():
                if version == 1:
                    resource_json = activity_json.get("resource")
                    member_json = activity_json.get("member")

                    if resource_json and member_json and type:
                        resource = Resource.get_or_create(
                            resource_json["id"],
                            resource_json["type"],
                            resource_json["data"],
                        )
                        member = Member.get_or_create(member_json)
                        activity = Activity(resource=resource, member=member, type=type)
                        db.session.add(activity)
                        db.session.commit()

            ch.basic_ack(delivery_tag=method.delivery_tag)
        except json.decoder.JSONDecodeError:
            logging.error("JSON is not correctly formatted. Not storing activity.")
            return
        except ValidationError as e:
            logging.error(e.messages)
            return
