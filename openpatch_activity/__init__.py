import os
from flask import Flask, jsonify
from threading import Thread
from instance.config import app_config
from openpatch_core.database import db
from openpatch_core.schemas import ma
from openpatch_core.errors import error_manager
from openpatch_activity.api.v1 import api as api_v1
from openpatch_activity.worker import Worker

RABBITMQ_HOST = os.getenv("RABBITMQ_HOST")
RETRY_DELAY_MS = os.getenv("RETRY_DELAY_MS", 30000)


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)
    ma.init_app(app)
    error_manager.init_app(app)

    # thread for receiving messages

    if config_name != "testing":
        worker = Worker(app, RABBITMQ_HOST, RETRY_DELAY_MS)
        thread = Thread(target=worker.consume)
        thread.daemon = True
        thread.start()

    # register api endpoints
    app.register_blueprint(api_v1, url_prefix="/v1")

    @app.route("/healthcheck", methods=["GET"])
    def healthcheck():
        return jsonify({"msg": "ok"}), 200

    return app
