# Activity Service

API Documentation: https://openpatch.gitlab.io/activity-backend

## Environment Variables

| Environment Variable | Description | Default | Values |
| -------------------- | ----------- | ------- | ------ |
| OPENPATCH_MOCK | Fill the database with mock data | not set | "true" |
| OPENPATCH_MODE | Configures the configuration | "development" | "development", "production", "testing" |
| OPENPATCH_DB | URI for connecting to a database | sqlite:///database.db | \<protocol\>://\<path\> |
| OPENPATCH_AUTHENTIFICATION_SERVICE | url to authentification service | https://api.openpatch.app/authentification | a url |
| OPENPATCH_ORIGINS | Allowed CORS origins | "*" | valid cors origin |
| SENTRY_DSN | Error tracking with Sentry | not set | a valid dsn |
| RABBITMQ_HOST | URI for connecting to rabbitmq | not set (required) | amqp://\<path\> |

## Start

```
docker-compose up
```

## Mock Data

```
docker-compose exec backend flask mock
```

## Testing

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend flask test
```

## Coverage

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend python3 coverage_report.py
```

## ER-Diagram

![Er-Diagram](.gitlab/er_diagram.png)
