## [1.1.4](https://gitlab.com/openpatch/activity-backend/compare/v1.1.3...v1.1.4) (2021-01-25)


### Bug Fixes

* upgrade flask-microservice-base to fix mysql connect issue ([dff81a3](https://gitlab.com/openpatch/activity-backend/commit/dff81a3ee81a40a58ae9ec42184b7d0558d780d2))

## [1.1.3](https://gitlab.com/openpatch/activity-backend/compare/v1.1.2...v1.1.3) (2020-06-06)


### Bug Fixes

* mocking of data ([ded4831](https://gitlab.com/openpatch/activity-backend/commit/ded4831256c91d53b9da2b6c2ea4637ac1e01b27))

## [1.1.2](https://gitlab.com/openpatch/activity-backend/compare/v1.1.1...v1.1.2) (2020-06-06)


### Bug Fixes

* default database ([7be444c](https://gitlab.com/openpatch/activity-backend/commit/7be444ccb9cb06de1ff08f4e089afeb4932d3901))
